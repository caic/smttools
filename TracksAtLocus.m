function AnalyseTracks
addpath 'D:\Projects\SMLM\akde'
addpath 'D:\Projects\SMT\vbSPT1.1.4_20170411\Tools\Misc'
addpath D:\Images\Gianluca\HS3D\Dual_TVL1_Optical_Flow-master
global minTraj 
global ly
global lx
minTraj  = 3;
nrstates = 3
%% make sure you are in the parent folder of the results folder
px = 110    % pixel size
[fn pt] = uigetfile('*.mat', 'Select vbspt mat file');
%%
[fn2 pt2] = uigetfile('*.tif', 'Select locus image');
im = imread(fullfile(pt2, fn2));
%im = []
1e-9
%% read in vbspt result
a = load(fullfile(pt, fn));

% Read in D for the found states
D_states = a.Wbest.est.DdtMean/a.options.timestep;
D2 = a.Wbest.est2.viterbi;
% Read in the trajectory data
a.options.dim = 2;
X = VB3_readData(a.options);
%%
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];
% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>'
lineWidth=1;
%% thresholding the locus image
t = multithresh(im-imgaussfilt(im,5),2)
locus = (im-imgaussfilt(im,5))>t(2);
figure; imagesc(locus); title('Locus')
% locus pixels
[ly lx] = find((im-imgaussfilt(im,5))>t(2));
% compute distance to locus
d = bwdist((im-imgaussfilt(im,5))>t(2));  title('Distance to locus')
figure; imagesc(d); title('Distance from closest locus pixel')
%% very long. Plot trajectories
all = [];

%col3 = [1 0 0; 1 1 0; 0 1 0; 0 0 1]
all = cellfun(@(x, y) [x(1:end,1:2) [x(2:end,1:2); 0 0] single([y; 0]) single([y(2:end); 0;0])], X, traj_state,'UniformOutput', 0);
lgall = cellfun(@(x) size(x,1), all);

allmat = cell2mat(all');
%% region with distance smaller/bigger than...
% distance in pixels from the locus
% if you replace in line 147: startpos>dst - outside the " near locus"
% region
dst = 10
%figure; hist(sd(id), 100)
%%

[idx d] = knnsearch([ly lx],[allmat(:,2)/px allmat(:,1)/px]);
figure; hist(d(:),100); title('Distance to locus - all localisations');
figure
for k = 1:3
   id = find(allmat(:,5)==k);
   subplot(1,3,k)
   hist(d(id),100); title(strcat('Distance to locus, D',num2str(k)));
end
%%
for s1 = 1:3    
   for s2 = 1:3 
   id = find((allmat(:,5)==s1)&(allmat(:,6)==s2));
   subplot(3,3,(s1-1)*3+s2)
   hist(d(id),100); title(strcat('Distance to locus: D', num2str(s1), '->D', num2str(s2)));
   end
end

%% 
xx = cell2mat(X');
gridx1 = min(xx(:,1)):100:max(xx(:,1));
gridx2 = min(xx(:,2)):100:max(xx(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
xi = [x1(:) x2(:)];

clear kd
ker = 500

figure;
for s1 = 1:3    
   for s2 = 1:3 
    id = find((allmat(:,5)==s1)&(allmat(:,6)==s2));
    subplot(3,3,(s1-1)*3+s2)
    kd{s1,s2} =  reshape(ksdensity(allmat(id,1:2),xi,'Bandwidth',ker), size(x1));   
    imagesc(kd{s1,s2});   
    title(strcat('Density maps: D', num2str(s1), '->D', num2str(s2)));
    colormap hot
    axis equal tight
    caxis(c)
   end 
end

%%
figure;
for k = 1:3
subplot(1,3,k)
id = find((qmode==k))
hist(lg(id),100); title(strcat('D', num2str(k) ,' - Length traj.'))
end

