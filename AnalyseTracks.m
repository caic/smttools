function AnalyseTracks
addpath 'D:\Projects\SMLM\akde'
addpath 'D:\Projects\SMT\vbSPT1.1.4_20170411\Tools\Misc'
addpath D:\Images\Gianluca\HS3D\Dual_TVL1_Optical_Flow-master
global minTraj 
minTraj  = 3;
nrstates = 3
%% make sure you are in the parent folder of the results folder
px = 110    % pixel size
[fn pt] = uigetfile('*.mat', 'Select vbspt mat file');
%%
[fn2 pt2] = uigetfile('*.tif', 'Select locus image');
im = imread(fullfile(pt2, fn2));
%im = []
%% read in vbspt result
a = load(fullfile(pt, fn));

% Read in D for the found states
D_states = a.Wbest.est.DdtMean/a.options.timestep;
D2 = a.Wbest.est2.viterbi;
% Read in the trajectory data
a.options.dim = 2;
X = VB3_readData(a.options);
%%
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];
% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>'
lineWidth=1;
%% thresholding the locus image
t = multithresh(im-imgaussfilt(im,5),2)
locus = (im-imgaussfilt(im,5))>t(2);
figure; imagesc(locus); title('Locus')
% locus pixels
[ly lx] = find((im-imgaussfilt(im,5))>t(2));
% compute distance to locus
d = bwdist((im-imgaussfilt(im,5))>t(2));  title('Distance to locus')
figure; imagesc(d); title('Distance from closest locus pixel')
%% compute asymmetries
lg = cellfun(@length, X);
%qc = cellfun(@median,traj_state);
qmode = cellfun(@mode, traj_state);
counts = cellfun(@length,traj_state);
qmode(counts<2) = [];
[a2 A2] = cellfun(@TrajAssym,X);

id  = ~isnan(a2)
figure; 
subplot(121);hist(a2(id), 30); title('Asymmetries of trajectories  (1)')
subplot(122);hist(A2(id), 30); title('Asymmetries of trajectories (2)')
%%

figure
for k = 1:max(unique(qmode))%:-1:1
    id = find(qmode==k);
    k
    subplot(2,2,double(k))
    hist(a2(id), 30);title(strcat('D=',num2str(D_states(k)/1000000,2)))
    %line([q(id,1)'/px; q(id,1)'/px+ q(id,3)'/px], [q(id,2)'/px; q(id,2)'/px+ q(id,4)'/px])
end
figure
for k = 1:max(unique(qmode))%:-1:1
    id = find(qmode==k);
    k
    subplot(2,2,double(k))
    hist(A2(id), 30);title(strcat('D=',num2str(D_states(k)/1000000,2)))
    %line([q(id,1)'/px; q(id,1)'/px+ q(id,3)'/px], [q(id,2)'/px; q(id,2)'/px+ q(id,4)'/px])
end


%% compute quiver data

q = zeros(stop-start+1,5);
for i = start:stop
    q(i,:) = [X{i}(1,1),X{i}(1,2),X{i}(end,1)-X{i}(1,1),X{i}(end,2)-X{i}(1,2),double(size(X{i},1))];
   
end

%% Color coded asymmetry
figure
for k = max(unique(qmode)):-1:1
    id = find(qmode==k);
    k
    subplot(2,2,double(k))
    imshow((locus),[]); hold on
    colormap bone
    scatter(q(id,1)/px,q(id,2)/px,5, A2(id)', 'filled');%, 'Color', col(k,:), 'LineWidth',1.5, 'AutoScale','off');  hold on    
    axis equal tight
    %alpha(0.6)
    title(strcat('D', num2str(k)))     
    Astate{k} = A2(id);
end
disp('KS test for asymmetries')
[h,p] = call_kstest2(Astate,0.01)
colormap jet
%% compute median state /trajectory
col = 'rygb'

%% very long. Plot trajectories
all = [];

col3 = [1 0 0; 1 1 0; 0 1 0; 0 0 1]
all = cellfun(@(x, y) [x(1:end-1,1:2) x(2:end,1:2) single(y) single([y(2:end); 0])], X, traj_state,'UniformOutput', 0);
lgall = cellfun(@(x) size(x,1), all);
lg = cellfun(@length, X);
%qc = cellfun(@median,traj_state);
qmode = cellfun(@mode, traj_state);
% for i = start:stop 
%     all = [all; X{i}(1:end,1),X{i}(1:end,2) double([traj_state{i};traj_state{i}(end)]) ];
% end

%%
figure
id = find(lg>100);
for k = 1:length(id)    
    scatter(X{id(k)}(:,1),X{id(k)}(:,2), 1, double([traj_state{id(k)};traj_state{id(k)}(end)])); hold on
end
colormap lines
%%
col= colormap('lines')
figure
if ~isempty(im)
    imshow(im, [])
end
hold on
% for k = 1:4
%     id1 = find(all(:,3)==k);
%     scatter(all(id1,1)/px,all(id1,2)/px,3, col(all(id1,3),:), 'MarkerSize', 0.5);%,1,col3(traj_state{i})'));
% end

% quiver(q(:,1)/px,q(:,2)/px,q(:,3)/px,q(:,4)/px,'LineWidth',1.5, 'AutoScale','off');%, 'Color', col(k,:), 'LineWidth',1.5, 'AutoScale','off');  hold on
%Plot quiver. Is the color right?
for k = max(unique(qmode)):-1:1
    id = find(qmode==k);
    k
    quiver(q(id,1)/px,q(id,2)/px,q(id,3)/px,q(id,4)/px,'LineWidth',1.5, 'AutoScale','off');%, 'Color', col(k,:), 'LineWidth',1.5, 'AutoScale','off');  hold on
    alpha(0.2)    
end
if nrstates == 4
    legend('D4 (fastest)','D3','D2','D1 (slowest)')
else
    legend('D3(fastest)','D2','D1 (slowest)')
end
axis equal tight

%% do tf's move towards the locus?
% compute signed distance sd - if a molecule moves to/away from locus
for k = 1:size(q,1)
    startpos(k) = d(max(1,floor(q(k,2)/px)),max(1,floor(q(k,1)/px)));
    endpos(k) = d(max(1,floor((q(k,2)+q(k,4))/px)),max(1,floor((q(k,1)+q(k,3))/px)) );
    %signed distance 
        % - positive dist. motion away from locus
        % - negative dist. motion towards from locus
    sd(k) =endpos(k)-startpos(k);    
end
%% region with distance smaller/bigger than...
% distance in pixels from the locus
% if you replace in line 147: startpos>dst - outside the " near locus"
% region
dst = 10
id = find(startpos<dst);
%figure; hist(sd(id), 100)

%%
% clear spos epos sdist
% %state by state
% lengthThresh = 5;% length of trajectory
% distThresh = 15  % distance to locus
% for s = max(unique(qmode)):-1:1
%     id = find((qmode==s)&(lg>lengthThresh));
%     ifar = find(startpos(id)>distThresh);
%     id(ifar ) = [];
%     %figure; hist(sd(id), 100)
%     size(id)
%     for k = 1:length(id)
%         
%         spos{s}(k) = d(max(1,floor(q(id(k),2)/px)),max(1,floor(q(id(k),1)/px)));
%         epos{s}(k) = d(max(1,floor((q(id(k),2)+q(id(k),4))/px)),max(1,floor((q(id(k),1)+q(id(k),3))/px)) );
%         %signed distance 
%         % - positive dist. motion away from locus
%         % - negative dist. motion towards from locus
%         sdist{s}(k) =epos{s}(k)-spos{s}(k);    
%     end
%     figure;
%     scatter(q(id,1)/px,q(id,2)/px,5, sdist{s}, 'filled' ); title(strcat('Signed distance for D',num2str(s)))
%     colorbar
%     colormap jet
%     %line([q(id,1)'/px; q(id,1)'/px+ q(id,3)'/px], [q(id,2)'/px; q(id,2)'/px+ q(id,4)'/px])
% end
%%
clear spos epos sdist
%state by state
lengthThresh = 5;% length of trajectory
distThresh = 25  % distance to locus
for s = max(unique(qmode)):-1:1
    s
    id = find((qmode==s)&(lg>lengthThresh));
    ifar = find(startpos(id)>distThresh);
    id(ifar ) = [];
    %figure; hist(sd(id), 100)
    [idx spos{s}] = knnsearch([ly lx],[q(id,2)/px q(id,1)/px]);
    [idx epos{s}] = knnsearch([ly lx],[(q(id,2)+q(id,4))/px (q(id,1)+q(id,3))/px]  );
    sdist{s} =epos{s}-spos{s};    
   
    figure;
    scatter(q(id,1)/px,q(id,2)/px, 5, sdist{s}, 'filled' ); title(strcat('Signed distance for D',num2str(s)))
    colorbar
    colormap jet
    % signed dist vs length of trajs
    plot(lg(id), sdist{s}, '.'); title(strcat('Signed distance for D',num2str(s), 'vs. traj. length'))
    %line([q(id,1)'/px; q(id,1)'/px+ q(id,3)'/px], [q(id,2)'/px; q(id,2)'/px+ q(id,4)'/px])
end

%%
figure; 
subplot(131); hist(sdist{1},100 ); title(strcat(num2str(mean(sdist{1})), ', slowest'))
subplot(132); hist(sdist{2},100 ); title(num2str(mean(sdist{2})))
subplot(133); hist(sdist{3},100 ); title(strcat(num2str(mean(sdist{3})), ', fastest'))
skewness(sdist{1})
skewness(sdist{2})
skewness(sdist{3})
median(sdist{1})
median(sdist{2})
median(sdist{3})


%% statistical test per state. Comparing the mean to 0
disp('state 1')
[h(1),pvalue(1),ci] = ttest(sdist{1},0) 
disp('state 2')
[h(2),pvalue(2),ci] = ttest(sdist{2},0)
disp('state 3')
[h(3),pvalue(3),ci] = ttest(sdist{3},0)
disp('Significance (S1,S2,S3)')
h
disp('p-values')
pvalue
%%
figure;
ecdf(sdist{1}); hold on
ecdf(sdist{2});
ecdf(sdist{3})
legend('1', '2', '3')


%% Density - localisations (possible tracks)
xx = cell2mat(X');
gridx1 = min(xx(:,1)):100:max(xx(:,1));
gridx2 = min(xx(:,2)):100:max(xx(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
xi = [x1(:) x2(:)];
%%
% figure
% %ksdensity(xx,xi);
% ksdensity(q(:,1:2),xi); title('Density map - all trajectories')
% shading interp
%%
clear kd
ker = 500
for s = max(unique(qmode)):-1:1
    id = find((qmode==s));
    kd{s} =  reshape(ksdensity(q(id,1:2),xi,'Bandwidth',ker), size(x1));         
end

% figure;
% surf(reshape(kd{1}, size(x1)),'FaceColor' ,'blue','FaceAlpha',0.5, 'EdgeColor','none'); hold on
% surf(reshape(kd{2}, size(x1)),'FaceColor' ,'cyan','FaceAlpha',0.5, 'EdgeColor','none')
% surf(reshape(kd{3}, size(x1)),'FaceColor' ,'magenta','FaceAlpha',0.5, 'EdgeColor','none')
% %surf([min(x2) max(x2)],[min(x1) max(x1)],repmat(0, [2 2]),im,'facecolor','texture')
% surf([1 size(x1,1)],[1 size(x2,1)],repmat(0, [2 2]),im,'facecolor','texture')
%% density maps

figure;
subplot(221)
imagesc(kd{1}); hold on; title('D1')
colormap hot
%shading interp
%surf([1 size(x1,1)],[1 size(x2,1)],repmat(0, [2 2]),im,'facecolor','texture')
subplot(222)
imagesc(kd{2}); hold on; title('D2')
%shading interp
%surf([1 size(x1,1)],[1 size(x2,1)],repmat(0, [2 2]),im,'facecolor','texture')
subplot(223)
imagesc(kd{3}); hold on; title('D3')
%shading interp
%surf([min(x2) max(x2)],[min(x1) max(x1)],repmat(0, [2 2]),im,'facecolor','texture')
subplot(224)
imshow(imresize(cropim,110/100), []); title('Locus')

figure;
subplot(311)
imshowpair(kd{1},imresize(cropim,110/100)); hold on; title('D1')
colormap hot
%shading interp
%surf([1 size(x1,1)],[1 size(x2,1)],repmat(0, [2 2]),im,'facecolor','texture')
subplot(312)
imshowpair(kd{2},imresize(cropim,110/100)); hold on; title('D2')
%shading interp
%surf([1 size(x1,1)],[1 size(x2,1)],repmat(0, [2 2]),im,'facecolor','texture')
subplot(313)
imshowpair(kd{3},imresize(cropim,110/100));hold on; title('D3')


%%
figure;
for k = 1:3
subplot(1,3,k)
id = find((qmode==k))
hist(lg(id),100); title(strcat('D', num2str(k) ,' - Length traj.'))
end
%%  Flows
% %% flow average
% clear f
% %[x y] = meshgrid(min(q(:,1)):200:max(q(:,1)),min(q(:,2)):200:max(q(:,2)));
% rv = RBF(q(:,1:2), q(:,3:4)./repmat(sqrt(sum(q(:,3:4).^2,2)),1,2), [x1(:) x2(:)], 500);
% 
% f(:,:,1) = reshape(rv{1}, size(x1));
% f(:,:,2) = reshape(rv{2}, size(x1));
% img = flowToColor(f)
% [vx_x,vx_xy ] = gradient(f(:,:,1));
% [vy_x,vy_y ] = gradient(f(:,:,2));
% figure
% imagesc (vx_x+vy_y);  title('Divergence')
% figure; imshow(img,[]); hold on;
% mask = imerode(kd{3}>0, strel('disk',31));
% Res = FDR((vx_x+vy_y).*mask, 0.05, 0, mask);
% subplot(121); 
% imshowpair(Res>0,imresize(cropim,110/100)); title(strcat('D', num2str(k), '-sources' ))
% subplot(122); 
% imshowpair(Res<0,imresize(cropim,110/100)); title(strcat('D', num2str(k), '-sinks' ))
% %%
% % 
% % [X,Y] = meshgrid(0:6,0:6);
% % U = X/sqrt(X.^2+Y.^2);
% % V = Y/sqrt(X.^2+Y.^2);
% % quiver(X,Y,U,V,0)
% % %%
% % Res = zeros(51, 51,2);
% % r = 10
% % theta = (0:1:360)/360*(2*pi);
% % for r = 10:12
% %     x1 = r*cos(theta)+26;
% %     y1 = r*sin(theta)+26;
% %     x2 = (r+2)*cos(theta);
% %     y2 = (r+2)*sin(theta);
% %     quiver(x1, y1, x2-x1, y2-y1)
% %     Res(floor(y1),floor(x1),2) = 1;
% %     Res(floor(y1),floor(x1),1) = 1;
% % end
% % wheel = flowToColor(Res);
% % figure; imshow(wheel,[])
% 
% spacing = 0.1;
% [X,Y] = meshgrid(-3:spacing:3);
% Z = -exp(-X.^2 - Y.^2);
% [DX,DY] = gradient(Z,spacing);
% quiver(DX, DY)
% wheel = flowToColor(cat(3, DX, DY));
% figure; imshow(wheel,[])
% %% flow per state
% cropim = im(max(1,floor(min(x2(:))/110)):floor(max(x2(:)/110)),max(1,floor(min(x1(:)/110))):max(floor(x1(:)/110)));
% croplocus = imresize(locus(max(1,floor(min(x2(:))/110)):floor(max(x2(:)/110)),max(1,floor(min(x1(:)/110))):max(floor(x1(:)/110))),110/100);
% [ yy xx] = find(bwperim(croplocus)>0);
% for k = max(unique(qmode)):-1:1
%     id = find((qmode==k)&(lg >0));
%     k 
%     % just total track orientation
%     rv = RBF(q(id,1:2), q(id,3:4)./repmat(sqrt(sum(q(id,3:4).^2,2)),1,2), [x1(:) x2(:)], 500);
%     % normalised displacement (with traj length)
%     % rv = RBF(q(id,1:2), q(id,3:4)./[lg(id)' lg(id)'], [x1(:) x2(:)], 500);
%     % unnormalised displacement
%     % rv = RBF(q(id,1:2), q(id,3:4), [x1(:) x2(:)], 500);
%     f(:,:,1) = reshape(rv{1}, size(x1));
%     f(:,:,2) = reshape(rv{2}, size(x1));
%     
% %     v1 = griddata(q(id,1),q(id,2),q(id,3),x1(:),x2(:));
% %     v2 = griddata(q(id,1),q(id,2),q(id,3),x1(:),x2(:));
% %     f(:,:,1) = reshape(v1, size(x1));
% %     f(:,:,2) = reshape(v1, size(x1));
%     f(1,1,1) = 1; f(end,end,1) = 1; f(1,end,1) = 1;f(end,1,1) = 1;
%     f(1,1,2) = 1; f(end,end,2) = 1; f(1,end,2) = 1;f(end,1,2) = 1;
%     img = flowToColor(f); 
%     figure;% imshowpair (img,imresize(im, size(f(:,:,1))));  title(num2str(k))
%     imshow (img);  title(num2str(k));hold on
%     plot(xx,yy,'k.')
%     figure;% imshowpair (img,imresize(im, size(f(:,:,1))));  title(num2str(k))
%     [vx_x,vx_xy ] = gradient(f(:,:,1));
%     [vy_x,vy_y ] = gradient(f(:,:,2));
%     diverg{k} = vx_x+vy_y;
%     imshowpair (vx_x+vy_y,imresize(cropim,110/100));  title(num2str(k)); hold on
%     
%     figure;
%     mask = imerode(kd{k}>0, strel('disk',31));
%     Res = FDR(diverg{k}.*mask, 0.05, 0, mask);
% %     subplot(121); imagesc(Res>0); title(strcat('D', num2str(k), '-sources' ))
% %     subplot(122); imagesc(Res<0); title(strcat('D', num2str(k), '-sinks' ))
%     %imshowpair(Res>0,Res<0); title(strcat('D', num2str(k), ': green-sources, magenta-sinks' ))
%     subplot(121); 
%     imshowpair(Res>0,imresize(cropim,110/100)); title(strcat('D', num2str(k), '-sources' ))
%     subplot(122); 
%     imshowpair(Res<0,imresize(cropim,110/100)); title(strcat('D', num2str(k), '-sinks' ))    
%     figure;imagesc(mask)
 end


 %kstest2
 %change significance level or type of alt hypothesis using "Name,Value"
 %unequal and alpha = 0.05 are the defaults 
 %can use "larger" or "smaller" 
 %k is the value of the difference between the ecdfs
 function[h,p] = call_kstest2(x,a);
 h = -1*ones(3,3);
 p = -1*ones(3,3);
 [h(1,2) p(1,2)] = kstest2(x{1},x{2},'alpha',a);
 [h(1,3) p(1,3)] = kstest2(x{1},x{3},'alpha',a);
 [h(2,3) p(2,3)] = kstest2(x{2},x{3},'alpha',a);

 end
 
function [a2 A2 ]= TrajAssym(x)
    global minTraj  
    if(size(x,1) >= minTraj)
        Txx = mean(x(:,1).^2)-mean(x(:,1))^2;
        Tyy = mean(x(:,2).^2)-mean(x(:,2))^2;
        Txy = mean(x(:,1).*x(:,2))-mean(x(:,1))*mean(x(:,2));
        R1 =  (1/2*(Txx+Tyy+sqrt((Txx-Tyy)^2+4*Txy^2)));
        R2 =  (1/2*(Txx+Tyy-sqrt((Txx-Tyy)^2+4*Txy^2)));
        a2 = R2/R1;
        A2 = (R1-R2).^2./(R1+R2).^2;
        % keep in mind R(1) is the R2 in the paper and R(2) is the R1 in
% the paper. Check PCA
%        R = eig([Txx Txy; Txy Tyy]);        
%        a2(i) = R(1)/R(2)
        
%        plot(X{i}(:,1),X{i}(:,2)); title(strcat('a2 = ',num2str(a2)))
%        axis equal tight
        %pause
    else
        a2 = nan;
        A2 = nan;
    end
end