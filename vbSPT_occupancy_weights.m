%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vbSPT occupancy correction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
%load vbSPT input track file
[fn1, pt1] = uigetfile(('*.mat'), 'Choose a File');
load(fn1)
%load vbSPT output track file
[fn2, pt2] = uigetfile(('*.mat'), 'Choose a File');
load(fn2)


%find number of trajectories
traj_no=length(chtrack);

%rename struct with hidden states
traj_state = Wbest.est2.sMaxP;

%find average length of tracks
for i = 1:length(traj_state)
    length_traj = length(traj_state{i});
    one(i) = sum(traj_state{i}(:) == 1)/length_traj;
    two(i) = sum(traj_state{i}(:) == 2)/length_traj;    
    three(i) = sum(traj_state{i}(:) == 3)/length_traj;    
end

total_sum = sum(one) + sum(two) + sum(three);

state_one_occupation = sum(one)/total_sum
state_two_occupation = sum(two)/total_sum
state_three_occupation = sum(three)/total_sum
