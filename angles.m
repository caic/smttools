%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Measure angles of each state and plot histograms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load vbSPT input track file
[fn1, pt1] = uigetfile(('*.mat'), 'Choose a File');
load(fn1)
%load vbSPT output track file
[fn2, pt2] = uigetfile(('*.mat'), 'Choose a File');
load(fn2)

%find number of trajectories
traj_no=length(chtrack);

%rename struct with hidden states
traj_state = Wbest.est2.sMaxP;

%add states to the cells
for i = 1 : traj_no
    track_length(i) = length(chtrack{i}(:,1));
    tracks_new{i} = chtrack{i}(:,1:2);
    tracks_new{i}(2:track_length(i),3) = traj_state{i};
end

data = zeros(100000,3);
for j = 1:traj_no
    S = sum(track_length(1:j));
    if track_length(j) >= 3
        for k = 2 : track_length(j)-1
        
            s1 = tracks_new{j}(k,3);
            s2 = tracks_new{j}(k+1,3);
        
            x1 = tracks_new{j}(k-1,1);
            x2 = tracks_new{j}(k,1);
            x3 = tracks_new{j}(k+1,1);
   
            y1 = tracks_new{j}(k-1,2);
            y2 = tracks_new{j}(k,2);
            y3 = tracks_new{j}(k+1,2);
            
            u = [x2-x1 y2-y1];
            v = [x3-x2 y3-y2];
        
            %CosTheta = max(min(dot(u,v)/(norm(u)*norm(v)),1),-1);
            %angle = real(acosd(CosTheta));
            
            CosTheta =(dot(u,v)/(norm(u)*norm(v)));
            angle = real(acos(CosTheta) * 180/pi);
            
            data(k+S,1) = s1;
            data(k+S,2) = s2;
            data(k+S,3) = angle;            
          
        end
    else
            
    end
end

%delete zero rows  
data(~any(data,2),:) = [];
data = rmmissing(data);

%save data for statistical tests on R
colNames = {'Initial_State','Final_State','Angle'};
sTable = array2table(data,'VariableNames',colNames);
writetable(sTable,strcat('angles_',strtok(fn1, '.'), '.csv')); 

%plot histograms
for k = 1 : 4
    data_k = data((data(:,1)==k & data(:,2)==k),:);
    
    no_lines = length(data_k(:,3));
    mu = mean(data_k(:,3));
    sigma = std(data_k(:,3));
    
    subplot(2,2,k);
    %figure
    histogram(data_k(:,3));
    grid on;
    
    xlabel('Angle', 'FontSize', 10);
    ylabel('Count', 'FontSize', 10);
    title({'State',k,'Line no.=',no_lines,'Mean=',mu,'std =', sigma},...
        'FontSize', 10);
end
    
 
   
  

