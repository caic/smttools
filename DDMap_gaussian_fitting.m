%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fit a gaussian mixture model to DDMap diffusions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%to run, save DDMap diffusion table as .mat file

%load THOT diffusion results
load('diffusion_synthetic_data_syntheticData_01.mat');

%extract diffusion column from THOT
diffusion = diffusion{:,:};
diffusion_column = diffusion(:,4);

%set no of components to be fitted
k = 2;

%fit Gaussian mixture model to data
fitgmdist(diffusion_column,k)
