%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test quality of data before trajectory analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath C:\Users\Harry\OneDrive\Documents\MATLAB\MATLAB\Systems Biology\Project\Data_Quality
folder = 'Data_Quality'
fname = 'r1_SuH_exp_50_ext_20_1.tif_loc_TS.csv'
imname = 'r1_SuH_exp_50_ext_20_1.tif'
dat = readtable(fullfile(folder, fname));
%%
Im = double(Read3d(fullfile(folder, imname)));
%%
px = 110

for k = 1:6000
    k
    id = find(dat.frame == k);
    nel(k) = length(id);
    if nel(k) > 1
    mTInt(k) = mean(dat.intensity_photon_(id) );
    sTInt(k) = std(dat.intensity_photon_(id) );
    
    I = Im(sub2ind(size(Im(:,:,1)), round(dat.y_nm_(id)/px), round(dat.x_nm_(id)/px)));
    mI(k) = mean(I);
    sI(k) = std(I);
    msnr(k) = mean(10*log10(I.^2./(dat.bkgstd_photon_(id).^2)));
    ssnr(k) = std(10*log10(I.^2./(dat.bkgstd_photon_(id).^2)));
    ylabel('dB')
    % knnsearch(t, t,2)
    % knnsearch(t, t+1,2)
    track = [dat.y_nm_(id),dat.x_nm_(id)];
    [nn d] = knnsearch(track, track, 'K',2);
    mnn(k) = mean(d(:,2));
    snn(k) = std(d(:,2));
    end
   
    if k>1
        idold = find(dat.frame == k-1);
        if length(idold)>1
        oldtrack = [dat.y_nm_(idold),dat.x_nm_(idold)];
        [nn2 d2] = knnsearch(track, oldtrack, 'K',1);
        
        %[mNextnn(k) d3] = knnsearch(track, oldtrack, 'K',1);
        mNextnn(k) = mean(d2);
        sNextnn(k) = std(d2);
        minNext(k) = min(d2);
        q(k, :) = quantile(d2, [0.01 0.05 0.10  0.25 0.5]);
        end
       
    else 
       mNextnn(1) = 0;
       sNextnn(1) = 0;
    end
    % nn = knnsearch(track, track, 'K',2);
   
end
%%
figure; 
subplot(311)
plot(nel(1:1000)); title('Localisations per frame');hold on;

mEl = mean(nel)

plot(nel, 'r')
xlabel('Frame number')
ylabel('Localisations')

subplot(312)

mI_remove_NaN = mI;
mI_remove_NaN(isnan(mI_remove_NaN))= []
mI_no_zeroes = mI;
mI_no_zeroes(mI_no_zeroes==0)=NaN

%Overall_mean_loc_intensity_per_frame = mean(mI_remove_NaN)
errorbar(mI_no_zeroes, sI); title('Localisation intensity per frame');hold on;
plot(mI_no_zeroes, 'r')
xlabel('Frame number')
ylabel('Localisation intensity')



subplot(313)

msnr_remove_NaN = msnr;
msnr_remove_NaN(isnan(msnr_remove_NaN))= []
msnr_no_zeroes = msnr;
msnr_no_zeroes(msnr_no_zeroes==0)=NaN
Overall_mean_SN_per_frame = mean(msnr_remove_NaN)
errorbar(msnr_no_zeroes, ssnr); title('S/N per frame');hold on;
plot(msnr_no_zeroes, 'r')
xlabel('Frame number')
ylabel('Signal-to-noise ratio')


subplot(514)
errorbar(mnn, snn); title('nn per frame'); hold on;
mnn_remove_NaN = mnn;
mnn_remove_NaN(isnan(mnn_remove_NaN))= []
Overall_mean_nn_per_frame = mean(mnn_remove_NaN)

plot(mnn, 'b')
ylabel('nm')

subplot(515)
plot(minNext, 'r')
xlabel('frames')
ylabel('nm')
%%
figure; plot(mnn, minNext, '.')
xlabel('Nearest neighbour same frame')
ylabel('Nearest neighbour next frame')

