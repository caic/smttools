addpath D:\Projects\Elia
mainFolder = 'D:\Projects\SMT\TrackIt';%fileparts(which(mfilename));
addpath(genpath(mainFolder));
addpath D:\Projects\SMT\TrackIt\subfun
addpath D:\Projects\SMT\TrackIt\u-track\software
%%
px_size = 110
%Im = Read3d('D:\Projects\SMT\data\13-05-2021\20210513_SuHHalo_locustag_Notch__4_MMStack_Pos0.ome.tif');
[fn folder]= uigetfile('*.csv');
%%
%data_wd=importdata(fullfile('D:\Projects\SMT\data\13-05-2021','13-05-2021-Notch4_TS.csv'));
data_wd=importdata(fullfile( folder, fn));
data_wd=data_wd.data;
%%
%params
            params.windowSize = 3
              params.minWidth = 0
              params.maxWidth= Inf
     params.maxRefinementDist= 2
           params.minSpotDist= 1
       params.thresholdFactor= 1
        params.trackingRadius= 825
        params.minTrackLength= 3
             params.gapFrames= 3
    params.minLengthBeforeGap= 0
            params.frameRange= [1001 max(data_wd(:,1))]
           params.stdFiltered= 14.4600
          params.intThreshold= 14.4600
        params.trackingMethod= 2
               params.version= 'TrackIt_v1_1_1'
            % params.timeStamp: 14-Sep-2021 10:33:55%
%% Create spors arrray            
for k = 1: max(data_wd(:,1))           
   id = find(data_wd(:,1)==k);
   %spots{k} = [dd(id,2)/px_size dd(id,3)/px_size dd(id,5) zeros(size(id))  3*ones(size(id)) 3*ones(size(id)) zeros(size(id)) ];
   spots{k} = [data_wd(id,2) data_wd(id,3) data_wd(id,5) zeros(size(id))  3*ones(size(id)) 3*ones(size(id)) zeros(size(id)) ];
end

%% utrack tracks

tracks = find_tracks(spots, params);
save(fullfile(folder, strcat('track_it',strtok(fn, '.'), '.mat')), 'tracks');
%%

ct0 = 1;
ct = 1;
for k = 1:length(tracks)
    chop = [0; find(table.blink(ii)==2); size(track{ct0},1)+1];
    if length(chop) ==2
        if size(track{ct0},1)>2
            chtrack{ct} =  track{ct0}(:,1:2); 
            ct = ct+1;
        end
    else
    for m =2:length(chop)
        if (chop(m)-chop(m-1))>3
            chtrack{ct} = [table.x(ii(chop(m-1)+1:chop(m)-1))*px table.y(ii(chop(m-1)+1:chop(m)-1))*px ];%table.blink(ii(chop(m-1)+1:chop(m)-1))];
            ct = ct+1;
        end
        
    end
    end
    ct0 =ct0+1;
    end
%%
%%ll = vertcat(cell2mat(cellfun(@size, tracks, 'UniformOutput', false)))